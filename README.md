# wlroots-async-pageflip
Patch to enable async pageflip on wlroots 0.16. This allows to use wayland without vsync, will enable tearing even on desktop. I took parts of code from https://gitlab.freedesktop.org/wlroots/wlroots/-/merge_requests/3871 and made it always enable async pageflip if its supported.

Async pageflip/tearing support is added in wlroots 0.17, this patch won't be updated, as now tearing can be implemented on compositor.

## How to use
1. Compile wlroots with this patch
2. Start your favorite wlroots compositor with WLR_DRM_NO_ATOMIC=1 environment variable.
3. Enjoy wayland without vsync

